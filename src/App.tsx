import React, { Suspense } from "react";
import "./App.css";

import { BrowserRouter, Switch, Route, Link } from "react-router-dom";
// import {Cube} from './journey/Cube';
const Cube = React.lazy(() => import("./journey/Cube"));
const Platform = React.lazy(() => import("./journey/Platform"));

function App() {
  return (
    <BrowserRouter>
      <div>
        <nav>
          <ol className="nav-item-list">
            <li>
              <Link to={"/"}>Home</Link>
            </li>
            <li>
              <Link to={"/cube"}>Cube</Link>
            </li>
            <li>
              <Link to={"/platform"}>Platform</Link>
            </li>
          </ol>
        </nav>
      </div>

      <Switch>
        <Suspense fallback={<div>Loading...</div>}>
          <Route path={"/cube"}>
            <Cube width={window.innerWidth} height={window.innerHeight} />
          </Route>
          <Route path={"/platform"}>
            <Platform width={window.innerWidth} height={window.innerHeight} />
          </Route>
          <Route path={"/"}>
            {null}
          </Route>
        </Suspense>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
