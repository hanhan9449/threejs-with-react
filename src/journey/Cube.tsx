import { useEffect, useRef } from "react";
import {
  BoxGeometry,
  Material,
  Mesh,
  MeshNormalMaterial,
  PerspectiveCamera,
  Scene,
  WebGLRenderer,
} from "three";

function Cube(props: { width: number; height: number }) {
  const containerEl = useRef(null);

  let camera: PerspectiveCamera;
  let scene: Scene;
  let renderer: WebGLRenderer;
  let geometry: BoxGeometry;
  let material: Material;
  let mesh: Mesh;

  function init() {
    camera = new PerspectiveCamera(70, props.width / props.height, 0.01, 10);
    camera.position.z = 1;

    scene = new Scene();

    geometry = new BoxGeometry(0.2, 0.2, 0.2);
    material = new MeshNormalMaterial();

    mesh = new Mesh(geometry, material);
    scene.add(mesh);

    renderer = new WebGLRenderer({ antialias: true });
    renderer.setSize(props.width, props.height);
    renderer.setAnimationLoop(animation);
    (containerEl.current as any).appendChild(renderer.domElement);
  }

  useEffect(() => {
    init();
  });

  function animation(time: number) {
    mesh.rotation.x = time / 2000;
    mesh.rotation.y = time / 1000;
    renderer.render(scene, camera);
  }

  return <div ref={containerEl} />;
}

export default Cube;
