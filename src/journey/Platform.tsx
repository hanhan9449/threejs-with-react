import { useEffect, useRef } from "react";
import {
  AxesHelper,
  BoxGeometry,
  Camera,
  Color,
  Mesh,
  MeshLambertMaterial,
  PerspectiveCamera,
  PlaneGeometry,
  Scene,
  SphereGeometry,
  SpotLight,
  Vector2,
  WebGLRenderer,
} from "three";
import * as dat from "dat.gui";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";

let ROTATION_SPEED = "rotationSpeed";
let BOUNCING_SPEED = "bouncingSpeed";
let CUBE_X_POSITION = "cubeXPosition";
let CUBE_Y_POSITION = "cubeYPosition";
let CUBE_Z_POSITION = "cubeZPosition";

function Platform(props: { width: number; height: number }) {
  const containerEl = useRef(null);

  let cube: Mesh;
  const controls = {
    [ROTATION_SPEED]: 0.05,
    [BOUNCING_SPEED]: 0.05,
    [CUBE_X_POSITION]: -4,
    [CUBE_Y_POSITION]: 3,
    [CUBE_Z_POSITION]: 0,
    step: 0,
  };
  useEffect(() => {
    let gui = new dat.GUI();
    gui.add(controls, ROTATION_SPEED, 0, 0.5);
    gui.add(controls, BOUNCING_SPEED, 0, 0.5);
    gui.add(controls, CUBE_X_POSITION, -10, 20);
    gui.add(controls, CUBE_Y_POSITION, -10, 20);
    gui.add(controls, CUBE_Z_POSITION, 0, 20);
    return () => {
      gui.destroy()
    }
  });

  function initCamera(): Camera {
    let camera = new PerspectiveCamera(45, props.width / props.height, 0.1, 1000);
    camera.position.set(-30, 40, 30);
    return camera;
  }

  function initRenderer(): WebGLRenderer {
    let renderer = new WebGLRenderer();
    renderer.setClearColor(new Color(0x000000));
    renderer.setSize(props.width, props.height);
    renderer.shadowMap.enabled = true;
    return renderer;
  }

  function initScene(): Scene {
    return new Scene();
  }

  function initPlane(): Mesh {
    let planeGeometry = new PlaneGeometry(60, 20);
    let planeMaterial = new MeshLambertMaterial({ color: 0xcccccc });
    let plane = new Mesh(planeGeometry, planeMaterial);

    plane.receiveShadow = true;
    plane.rotation.x = -0.5 * Math.PI;
    plane.position.set(15, 0, 0);

    return plane;
  }

  function initCube() {
    let cubeGeometry = new BoxGeometry(4, 4, 4);
    let cubeMaterial = new MeshLambertMaterial({
      color: 0xff0000,
    });
    let cube = new Mesh(cubeGeometry, cubeMaterial);
    cube.castShadow = true;
    cube.position.set(controls[CUBE_X_POSITION], controls[CUBE_Y_POSITION], controls[CUBE_Z_POSITION]);
    return cube;
  }

  function initSphere() {
    let sphereGeometry = new SphereGeometry(4, 20, 20);
    let sphereMaterial = new MeshLambertMaterial({
      color: 0x7777ff,
    });
    let sphere = new Mesh(sphereGeometry, sphereMaterial);

    sphere.castShadow = true;
    sphere.position.set(20, 4, 2);
    return sphere;
  }

  function initSpotLight() {
    let spotLight = new SpotLight(0xffffff);
    spotLight.position.set(-40, 40, -15);
    spotLight.castShadow = true;
    spotLight.shadow.mapSize = new Vector2(1024, 1024);
    spotLight.shadow.camera.far = 130;
    spotLight.shadow.camera.near = 40;
    return spotLight;
  }

  function init() {
    let scene = initScene();
    let camera = initCamera();
    let renderer = initRenderer();

    let axes = new AxesHelper(20);
    scene.add(axes);
    let plane = initPlane();
    scene.add(plane);
    cube = initCube();
    scene.add(cube);
    let sphere = initSphere();
    scene.add(sphere);

    let spotLight = initSpotLight();
    scene.add(spotLight);
    camera.lookAt(scene.position);

    ((containerEl.current as unknown) as HTMLElement).appendChild(renderer.domElement);
    renderer.setAnimationLoop(animation);
    let orbitControls = new OrbitControls(camera,renderer.domElement)
    orbitControls.maxPolarAngle = Math.PI / 2
    // orbitControls.minDistance = 1000
    // orbitControls.maxDistance = 5000

    function animation(_time: number) {
      controls.step += controls.bouncingSpeed;
      sphere.position.x = 20 + 10 * Math.cos(controls.step);
      sphere.position.y = 2 + 10 * Math.abs(Math.sin(controls.step));
      cube.rotation.y += controls.rotationSpeed;
      cube.position.set(controls[CUBE_X_POSITION], controls[CUBE_Y_POSITION], controls[CUBE_Z_POSITION]);
      renderer.render(scene, camera);
    }
  }

  useEffect(() => {
    init();
  });
  return <div ref={containerEl} />;
}

export default Platform;
